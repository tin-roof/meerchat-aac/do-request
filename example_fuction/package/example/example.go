package main

import (
	"encoding/json"

	"gitlab.com/tin-roof/meerchat-aac/request"
)

// Response to send back to the user
type Response struct {
	StatusCode int               `json:"statusCode,omitempty"`
	Headers    map[string]string `json:"headers,omitempty"`
	Body       string            `json:"body,omitempty"`
}

// resp is the local response to build out the object to be returned in the Response.Body
// this is setup to just have the fields of the response.Response to show that the fields exist
type resp struct {
	Headers         map[string]string `json:"headers"`
	IsBase64Encoded bool              `json:"isBase64Encoded"`
	Body            string            `json:"body"`
	Method          string            `json:"method"`
	Path            string            `json:"path"`

	ParsedBody Body    `json:"parsedBody"`
	Errors     []error `json:"errors"`
	OG         string  `json:"og"`
}

// Body defines the the type that would come in the POST request json body
type Body struct {
	Field string `json:"field"`
}

// Example is the function that runs when the endpoint is hit
// for the request package to work properly and to get all the info hidden behind
// the serverless architecture this `in` param needs to be map[string]interface{}
func Example(in map[string]interface{}) (*Response, error) {
	// define the request body type
	var cb Body

	// process the request passing in the option to set the body, this option
	// is what puts the request body into the body type defined above
	ctx, req, errs := request.Process(in, request.SetBody(&cb))

	// marshal the original request just so we can see what was passed in to the function
	og, _ := json.Marshal(in)

	// build the response with all the fields from the processed request
	res := resp{
		Body:            string(req.Body),
		IsBase64Encoded: req.IsBase64Encoded,
		Headers:         req.Headers,
		Method:          req.Method,
		Path:            req.Path,

		ParsedBody: cb,
		Errors:     errs,
		OG:         string(og),
	}
	resB, _ := json.Marshal(res)

	// return the response
	return &Response{
		Body: string(resB),
	}, nil
}
