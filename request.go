package request

import (
	"context"
	"encoding/json"
	"errors"

	"github.com/google/uuid"
)

type ContextField string

const (
	ContextFieldRequestID ContextField = "requestID"
)

const (
	// common request headers
	AcceptHeader          = "accept"
	AcceptEncodingHeader  = "accept-encoding"
	AuthorizationHeader   = "authorization"
	CacheControlHeader    = "cache-control"
	ContentTypeHeader     = "content-type"
	HostHeader            = "host"
	UserAgentHeader       = "user-agent"
	XForwardedForHeader   = "x-forwarded-for"
	XForwardedProtoHeader = "x-forwarded-proto"
	XRequestIDHeader      = "x-request-id"
)

type Request struct {
	Body            []byte                 `json:"body"`
	Data            map[string]interface{} `json:"query"`
	Headers         map[string]string      `json:"headers"`
	IsBase64Encoded bool                   `json:"isBase64Encoded"`
	Method          string                 `json:"method"`
	Path            string                 `json:"path"`
}

func Process(in map[string]interface{}, options ...RequestOption) (context.Context, Request, []error) {
	var req Request
	var errs []error

	// setup a context for the request
	ctx := context.Background()
	ctx = context.WithValue(ctx, ContextFieldRequestID, uuid.New().String())

	// loop over each of the properties and handle them appropriately
	for key, value := range in {
		switch key {
		case "__ow_body":
			body, ok := value.([]byte)
			if !ok {
				bByte, err := json.Marshal(value)
				if err != nil {
					errs = append(errs, err)
					continue
				}
				body = bByte
			}

			req.Body = body
		case "__ow_headers":
			headers, ok := value.(map[string]interface{})
			if !ok {
				errs = append(errs, errors.New("failed to parse headers"))
				continue
			}
			req.Headers = toMapOfStrings(headers)
		case "__ow_isBase64Encoded":
			ib64, ok := value.(bool)
			if !ok {
				errs = append(errs, errors.New("failed to parse isBase64Encoded"))
				continue
			}
			req.IsBase64Encoded = ib64
		case "__ow_method":
			method, ok := value.(string)
			if !ok {
				errs = append(errs, errors.New("failed to parse method"))
				continue
			}
			req.Method = method
		case "__ow_path":
			path, ok := value.(string)
			if !ok {
				errs = append(errs, errors.New("failed to parse path"))
				continue
			}
			req.Path = path
		default:
			// skip the http section because we just want the clean body data
			if key == "http" {
				continue
			}

			if len(req.Data) == 0 {
				req.Data = make(map[string]interface{})
			}

			req.Data[key] = value
		}
	}

	// create a body string from the query
	// this contains both query params and json body data
	dByte, err := json.Marshal(req.Data)
	if err != nil {
		errs = append(errs, err)
	}
	req.Body = dByte

	// run the options
	for _, option := range options {
		if err := option(&req); err != nil {
			errs = append(errs, err)
		}
	}

	return ctx, req, errs
}
