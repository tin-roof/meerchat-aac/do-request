package request

import (
	"encoding/base64"
	"encoding/json"
)

type RequestOption func(value *Request) error

// SetBody takes the json body string and parses it into the provided type, `bodyType` should be a pointer to a variable
func SetBody(bodyType interface{}) RequestOption {
	return func(req *Request) error {
		// no body to process
		if len(req.Body) == 0 {
			return nil
		}

		body := req.Body

		// decode the body if it is encoded
		if req.IsBase64Encoded {
			decoded, err := base64.StdEncoding.DecodeString(string(req.Body))
			if err != nil {
				return err
			}
			body = decoded
		}

		if err := json.Unmarshal(body, bodyType); err != nil {
			return err
		}

		return nil
	}
}

// SetQuery takes the query parameters and parses them into the provided type, `queryType` should be a pointer to a variable
func SetQuery(queryType interface{}) RequestOption {
	return func(req *Request) error {
		// no query params to process
		if req.Data == nil || len(req.Data) == 0 {
			return nil
		}

		paramB, err := json.Marshal(req.Data)
		if err != nil {
			return err
		}

		if err := json.Unmarshal(paramB, queryType); err != nil {
			return err
		}

		return nil
	}
}
