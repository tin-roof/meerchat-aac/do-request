package request

import "fmt"

// convert a map of interfaces to a map of strings
func toMapOfStrings(in map[string]interface{}) map[string]string {
	sm := make(map[string]string)
	for key, value := range in {
		if val, ok := value.(string); ok {
			sm[key] = val
		} else {
			sm[key] = fmt.Sprintf("%v", val)
		}
	}
	return sm
}
