# request

The request package is used to make working with the input param on Digital Ocean serverless functions in Golang a lot easier. It parses the request `map[string]interface{}` input type into a defined structure to make it easier to get things like request headers, URL query params, post body details.

## Example

There is an example inside the `example/` directory of how to use the request package to make a serverless function
